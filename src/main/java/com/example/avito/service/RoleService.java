package com.example.avito.service;

import com.example.avito.entity.Role;

import java.util.Collection;

public interface RoleService {
    Role getUserRole();
    Role getAdminRole();
}
